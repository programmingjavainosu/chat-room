import processing.core.PApplet;
import processing.core.PVector;

public class ChatRoomUI extends PApplet {

    final int MAX_NUMBER_OF_USER = 10;
    final int BACKGROUND_COLOR = 255;
    final int SCREEN_WIDTH = 1024;
    final int SCREEN_HEIGHT = 1024;
    final int USER_R = 30;
    final int USER_INPUT = 20;
    final float userSpeed = (float) 10.0;

    public User[] allData;
    public User mine;
    public int myID;

    public PVector camera;
    public StringBuffer currentBuffer;

    private boolean onClick;

    public ChatRoomUI(String name, int color, int myID) {
        super();

        this.allData = new User[this.MAX_NUMBER_OF_USER];
        for (int i = 0; i < this.MAX_NUMBER_OF_USER; i++) {
            this.allData[i] = new User();
        }

        this.myID = myID;

        this.mine = new User();
        this.mine.setName(name);
        this.mine.setColor(color);
        this.mine.setX(this.SCREEN_WIDTH / 2);
        this.mine.setY(this.SCREEN_HEIGHT / 2 + this.USER_INPUT);
        this.mine.setOnline(true);
        this.mine.newMessage("Hello! My name is " + name);

        this.camera = new PVector();
        this.camera.x = this.mine.getX() - this.width;
        this.camera.y = this.mine.getY() - this.height;

        this.currentBuffer = new StringBuffer();

    }

    public void startUp() {
        PApplet.main(new String[] { "--present", "ChatRoomUI" });
    }

    //drawing method
    @Override
    public void setup() {
        this.size(1024, 768);

    }

    @Override
    public void draw() {
        //clear everthing
        this.background(this.BACKGROUND_COLOR);

        //draw top text
        this.textSize(14);
        this.fill(0, 102, 153);
        this.text("FPS:" + this.frameRate, 10, this.USER_INPUT);
        this.textSize(20);
        this.text("Chat Room Created By Weicheng Huang; Server By Haifan Ou",
                200, this.USER_INPUT);

        //draw a isolation line
        this.stroke(150);
        this.strokeWeight(3);
        this.line(0, this.USER_INPUT + 10, this.width, this.USER_INPUT + 10);

        //draw everyone and their message
        for (int i = 0; i < this.MAX_NUMBER_OF_USER; i++) {
            if (this.allData[i].isOnline) {
                this.fill(this.allData[i].getColor());
                this.ellipse(this.allData[i].getX(), this.allData[i].getY(),
                        this.USER_R, this.USER_R);
                this.fill(25);
                this.textSize(10);
                this.text(
                        this.allData[i].getName() + ":"
                                + this.allData[i].getCurrentMessage(),
                                this.allData[i].getX(), this.allData[i].getY() - 10);
            }
        }

    }

    @Override
    public void mousePressed() {
        this.onClick = true;
    }

    @Override
    public void mouseReleased() {
        this.onClick = false;
    }

    /*
     * @Override public void mouseClicked() { this.mine.setX(this.mouseX);
     * this.mine.setY(this.mouseY);
     * 
     * }
     */
    public void updateAllVector(PVector[] vectors) {
        for (int i = 0; i < this.MAX_NUMBER_OF_USER; i++) {
            this.allData[i].setX((int) vectors[i].x);
            this.allData[i].setY((int) vectors[i].y);
        }
    }

    public void newMessage(String msg, int UserID) {
        this.allData[UserID].newMessage(msg);
    }
}
