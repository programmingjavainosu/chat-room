public class User {
    final int MAX_CACHE_MESSAGE = 50;

    public int x, y;
    public String[] message;
    public String currentMessage;
    public String name;
    public boolean isOnline;
    public int color;

    private int index;

    public User() {
        this.x = 0;
        this.y = 0;
        this.isOnline = false;
        this.dumpMessage();
        this.name = "Unknow Name";
        this.color = 150;
    }

    public boolean isOnline() {
        return this.isOnline;
    }

    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    //getter and setter
    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String[] getMessage() {
        return this.message;
    }

    public void setMessage(String[] message) {
        this.message = message;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return this.color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    //public mulitplate method
    public String getCurrentMessage() {
        return this.currentMessage;
    }

    public void dumpCurrentMessage() {
        this.currentMessage = null;
    }

    public void newMessage(String msg) {
        if (this.index >= this.MAX_CACHE_MESSAGE) {
            this.dumpMessage();
        }
        this.message[this.index] = msg;
        this.currentMessage = msg;
        this.index++;
    }

    public void dumpMessage() {
        this.index = 0;
        this.message = new String[this.MAX_CACHE_MESSAGE];
    }

}
